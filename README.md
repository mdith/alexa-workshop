# Alexa Skill Vorlage

## Beschreibung
Skillvorlage für den Hausmesse Alexa Workshop in Node.js.

## Abhängigkeiten
Keine

## Anforderungen:
* Node.js mit npm (Node Paketmanager) ist installiert
* Amazon Developer Account
* Amazon AWS Account

Optional:
* ASK CLI (>= v1.1.0) installiert und konfiguriert (siehe VK Confluence -> Innovation Campus -> Alexa B2B -> Technische Doku)


## Deployment auf AWS 
### Abhängigkeiten installieren
Im Ordner, in dem das package.json (Beschreibung von allen Abhängigkeiten) folgenden Befehl ausführen 
```
npm install
```

### Projektordner zippen 
Kompletten Projektordner mit heruntergeladenen NPM Abhängigkeiten zippen. Der Ordner node_modules muss auch im .zip vorhanden sein.

### Hochladen auf AWS Lambda
Das .zip Element nun auf AWS Lambda hochladen, speichern und testen ob der Code funktioniert. :)