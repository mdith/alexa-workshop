'use strict';
const Alexa = require('alexa-sdk');
const intentHandlers = require('./intentHandlers');

exports.handler = (event, context) => {
    var alexa = Alexa.handler(event, context);
    alexa.appId = undefined; // Optional: Alexa Skill Id hier einfügen
    alexa.registerHandlers(intentHandlers);
    alexa.execute();
};