'use strict'
let intentHandlers = {
    'LaunchRequest': function () {
        this.emit(':ask', 'Willkomen beim Alexa Beispiel Skill');
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', 'Help Intent');
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', 'Cancel Intent');
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', 'Stop Intent');
    },
    'SessionEndedRequest': function () {
        this.emit(':tell', 'SessionEnded Intent');
    },
    'Unhandled': function () {
        this.emit(':ask', 'Invalid Intent');
    },
};

module.exports = intentHandlers;